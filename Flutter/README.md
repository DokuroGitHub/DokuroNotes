# Install Flutter not using Android Studio

## folder structure

```bash
- C:\Dev
 - android_sdk
  - build-tools
  - cmdline-tools
  - emulator
  - licenses
  - platform-tools
  - platforms
  - emulator
 - flutter
```

## environment variables

```bash
Users variables:
path:
C:\Dev\flutter\bin

System variables:
Path:
C:\Dev\android_sdk\cmdline-tools\latest\bin
C:\Dev\android_sdk\emulator
C:\Dev\android_sdk\platform-tools
JAVA_HOME: C:\Dev\Java\jdk-20
```

## flutter

```bash
curl https://storage.googleapis.com/flutter_infra_release/releases/stable/windows/flutter_windows_3.10.0-stable.zip --output flutter.zip
```

## cmdline-tools (sdkmanager)

```bash
curl https://dl.google.com/android/repository/commandlinetools-win-9477386_latest.zip --output sdkmanager.zip
```

## sdkmanager common cmds

```bash
sdkmanager --update
sdkmanager --list_installed
```

## build-tools

```bash
sdkmanager "build-tools;33.0.2"
```

## platform-tools

```bash
sdkmanager "platform-tools"
```

## platforms

```bash
sdkmanager "platforms;android-33"
```

## licenses

```bash
sdkmanager --licenses
```

## flutter doctor --android-licenses

```bash
flutter doctor --android-licenses
```

## flutter doctor -v

```bash
flutter doctor -v
```

## optionals

## emulator

```bash
sdkmanager "emulator"
emulator -list-avds
```
